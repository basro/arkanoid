import Paddle from './Paddle';
import Ball from './Ball';
import Brick from './Brick';
import Rectangle from '../base/Rectangle';
import { vec2 } from 'gl-matrix';
import { CollisionResult, checkDiscBoxCollision } from '../base/colliders/Collision';
import { Level, Levels } from './Level';
import { clamp } from '../base/Math';
import { filterInPlace } from '../base/Misc';


const collision = new CollisionResult();
const PADDLE_SPEED = 100;
const BALL_START_SPEED = 130;
const BALL_MAX_SPEED = 170;
const BALL_SPEED_INCREMENT = 1;

interface PlayerInput {
	left: boolean,
	right: boolean,
}

type StateName = "playing" | "win" | "lose";

export class Gameplay {
	playerInput: PlayerInput = {left: false, right: false};
	paddle = new Paddle();
	ball = new Ball();
	bricks: Brick[] = [];
	playArea = new Rectangle(8,8,160,206);
	lives: number = 3;
	score: number = 0;
	state: StateName = "playing";
	ballShootSpeed = BALL_START_SPEED;

	onBallBounce?: (kind: number) => void;

	reset(level: Level) {
		this.resetPaddleAndBall();
		this.bricks = [];
		let x = this.playArea.x;
		let y = this.playArea.y;
		for ( let brickRow of level.bricks ) {
			for ( let kind of brickRow ) {
				if ( kind != null ) {
					const brick = new Brick(x + 8,y + 4,kind);
					this.bricks.push(brick);
				}
				x += 16;
			}
			x = this.playArea.x;
			y += 8;
		}
	}

	resetPaddleAndBall() {
		this.state = "playing";
		this.playerInput.left = false;
		this.playerInput.right = false;
		this.ballShootSpeed = BALL_START_SPEED;
		vec2.set(this.paddle.collider.position, 88, 188);
		this.resetBall();
	}

	resetBall() {
		vec2.set(this.ball.vel,1,-3);
		vec2.normalize(this.ball.vel, this.ball.vel);
		vec2.scale(this.ball.vel, this.ball.vel, this.ballShootSpeed);
		const pos = this.ball.collider.position;
		vec2.copy(pos, this.paddle.collider.position);
		pos[1] -= 8;
	}

	update(dt: number) {
		if ( this.state != "playing" ) {
			return;
		}

		const paddlePos = this.paddle.collider.position;
		if ( this.playerInput.left ) {
			paddlePos[0] -= PADDLE_SPEED * dt;
		}
		if ( this.playerInput.right ) {
			paddlePos[0] += PADDLE_SPEED * dt;
		}
		
		const paddleHalfWidth = this.paddle.collider.halfSize[0];
		paddlePos[0] = clamp(paddlePos[0], this.playArea.x + paddleHalfWidth, this.playArea.x + this.playArea.width - paddleHalfWidth);

		this.ball.update(dt, this);

		const ballVel = this.ball.vel;
		if ( ballVel[1] >= 0 && checkDiscBoxCollision(this.ball.collider, this.paddle.collider, collision) ) {
			let xdiff = this.ball.collider.position[0] - paddlePos[0];
			xdiff /= this.paddle.collider.halfSize[0];

			vec2.set(ballVel, xdiff, -0.5);
			vec2.normalize(ballVel, ballVel);
			vec2.scale(ballVel, ballVel, this.ballShootSpeed);

			this.ballShootSpeed = Math.min(this.ballShootSpeed + BALL_SPEED_INCREMENT, BALL_MAX_SPEED);

			if ( this.onBallBounce ) this.onBallBounce(0);
		}

		for ( let brick of this.bricks ) {
			if ( checkDiscBoxCollision(this.ball.collider, brick.collider, collision) ) {
				this.ball.respondToCollision(collision);
				this.score += 10;
				brick.hits--;
				brick.lastHitTime = performance.now();
				if ( this.onBallBounce ) this.onBallBounce(1);
			}
		}

		filterInPlace(this.bricks, brick => brick.hits > 0);

		if ( this.bricks.length == 0 ) {
			this.state = "win";
		}
		else if ( this.ball.collider.position[1] > this.playArea.bottom ) {
			this.state = "lose";
		}
	}
}
