import { Images, loadImages } from './Images'
import { Sounds, loadSounds } from './Sounds'
import { Graphics } from '../base/Graphics';
import { Gameplay } from './Gameplay';
import { playSound } from '../base/Audio';
import { coro } from '../base/Coroutines';
import { Input } from '../base/Input';

export type PongActions = "p1_up" | "p1_down" | "p2_up" | "p2_down";

export class Pong {
	private fsm: Generator;
	private dt: coro.DeltaTime = {value: 0};
	private renderFunc = function() {};
	private input = new Input<PongActions>();
	private game = new Gameplay();
	private score = [0,0];

	constructor( private gfx: Graphics, private audio: AudioContext, public images: Images, public sounds: Sounds ) {
		this.fsm = this.mainRoutine();

		const {input, game} = this;

		this.input.bind("ArrowUp", "p2_up");
		this.input.bind("ArrowDown", "p2_down");
		this.input.bind("KeyW", "p1_up");
		this.input.bind("KeyS", "p1_down");

		input.action("p1_up").onChange(value => {game.input[0].up = value});
		input.action("p1_down").onChange(value => game.input[0].down = value);
		input.action("p2_up").onChange(value => game.input[1].up = value);
		input.action("p2_down").onChange(value => game.input[1].down = value);

		input.start();

		game.onBallBounce = () => playSound(audio, sounds.bounce);
	}

	static async create(gfx: Graphics, audio: AudioContext): Promise<Pong> {
		const [images, sounds] = await Promise.all([loadImages(gfx), loadSounds(audio)]);
		return new Pong(gfx, audio, images, sounds);
	}

	update(dt: number) {
		this.dt.value = dt;
		this.fsm.next();
	}

	private *mainRoutine() {
		const {dt, game, audio, sounds} = this;

		let direction = 1;
		while ( true ) {
			game.reset(direction);

			yield* coro.waitForSeconds(dt, 1);

			while( game.state == "playing" ) {
				game.update(dt.value);
				yield;
			}

			playSound(audio, sounds.death);

			if ( game.state == "p1_score" ) {
				direction = -1;
				this.score[0]++;
			}else {
				direction = 1;
				this.score[1]++;
			}

			yield* coro.waitForSeconds(dt,1);
		}
	}

	render() {
		const {gfx, images, game} = this;
		gfx.resetTransform();
		gfx.translate(gfx.width >> 1, gfx.height >> 1);

		const scaleY = gfx.height / game.playArea.height;
		const scaleX = gfx.width / game.playArea.width;
		const scale = Math.min(scaleX, scaleY);
		gfx.scale(scale, scale);

		gfx.clear(0,0,0);

		// draw middle line
		gfx.tint(1,1,1,0.5);
		gfx.drawRect(-1,game.playArea.top, 2, game.playArea.height);
		gfx.tint(1,1,1,1);

		// draw ball
		const {position: bpos, radius: bradius} = game.ball.collider;
		gfx.drawImage(images.ballTex, bpos[0] - bradius, bpos[1] - bradius, bradius * 2, bradius * 2);

		// draw paddles
		for ( let paddle of game.paddles ) {
			gfx.drawRect(paddle.left, paddle.top, paddle.width, paddle.height);
		}

		// draw score
		gfx.drawTextCentered(images.font, `${this.score[0]}   ${this.score[1]}`, 0, game.playArea.top + 10);
	}
}
