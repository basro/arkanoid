import { loadSound } from '../base/Audio';

export interface Sounds {
	start: AudioBuffer;
	death: AudioBuffer;
	bounce1: AudioBuffer;
	bounce2: AudioBuffer;
	bounce3: AudioBuffer;
}

export async function loadSounds(ctx: AudioContext): Promise<Sounds> {
	const [start, death, bounce1, bounce2, bounce3] = await Promise.all(
		["game-start.ogg", "death.ogg", "bounce1.ogg", "bounce2.ogg", "bounce3.ogg"]
			.map(name => loadSound(ctx, "sounds/"+name))
	);
	return {start, death, bounce1, bounce2, bounce3};
}