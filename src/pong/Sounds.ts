import { loadSound } from '../base/Audio';

export interface Sounds {
	death: AudioBuffer;
	bounce: AudioBuffer;
}

export async function loadSounds(ctx: AudioContext): Promise<Sounds> {
	const [bounce, death] = await Promise.all(
		["bounce1.ogg", "death.ogg"]
			.map(name => loadSound(ctx, "sounds/"+name))
	);
	return {bounce, death};
}