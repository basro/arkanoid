import { Gameplay } from './Gameplay';
import { vec2 } from 'gl-matrix';
import Disc from '../base/colliders/Disc';
import { CollisionResult } from '../base/colliders/Collision';
import { vec2Sign } from '../base/Math';

export default class Ball {
	collider: Disc = new Disc(0,0,2.5);
	vel = vec2.create();

	update(dt: number, game: Gameplay) {
		const {position: pos, radius} = this.collider;
		const vel = this.vel;
		vec2.scaleAndAdd(pos, pos, vel, dt);

		let bounced = false;
		const playArea = game.playArea;
		if ( vel[0] < 0 ) {
			if ( pos[0] - radius < playArea.left ) {
				vel[0] *= -1;
				bounced = true;
			}
		}else {
			if ( pos[0] + radius > playArea.right ) {
				vel[0] *= -1;
				bounced = true;
			}
		}

		if ( vel[1] < 0 ) {
			if ( pos[1] - radius < playArea.top ) {
				vel[1] *= -1;
				bounced = true;
			}
		}

		if ( bounced && game.onBallBounce ) game.onBallBounce(2);
	}

	respondToCollision( collision: CollisionResult ) {
		const pos = this.collider.position;
		const vel = this.vel;
		const normal = collision.normal;
		vec2.scaleAndAdd(pos, pos, normal, collision.penetrationDistance);
		const nvel = vec2.dot(normal, vel);
		if ( nvel < 0 ) {
			vec2.scaleAndAdd(vel, vel, normal, -2 * nvel);
			this.preventHorizontalSpeeds();
		}
	}

	preventHorizontalSpeeds() {
		const vel = this.vel;
		const ratio = Math.abs(vel[0] / vel[1]);
		const maxRatio = 3;
		vec2Sign(tmpSign, vel);

		if ( ratio > maxRatio ) {
			const speed = vec2.len(vel);
			vec2.set(vel, maxRatio * tmpSign[0], 1 * tmpSign[1]);
			vec2.normalize(vel, vel);
			vec2.scale(vel, vel, speed);
		}
	}
}

const tmpSign = vec2.create();