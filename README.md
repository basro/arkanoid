# Arkanoid 

Quick and hopefully not too dirty HTML5 implementation of a simple Arkanoid-like game using WebGL.

[Play it here](https://basro.gitlab.io/arkanoid/)

Use left and right keyboard arrow keys to move the paddle.

### Building

You will need [NodeJS](https://nodejs.org/en/download/).

Clone the project

```
git clone https://gitlab.com/basro/arkanoid.git
cd arkanoid
```

Then in the root folder run

```
npm install
npm run-script build
```

This will create a folder named public with the bundled game files.

During development it's more convenient to watch for file changes using:

```
npx webpack --watch
```

### Running

You need a server in order to run the game locally.

Install http-server
```
npm install --global http-server
```

Then serve the files

```
npx http-server ./public/
```

# Project Structure

## static folder

The ./static folder contains all assets and all of its contents are simply copied to the public folder on build.

## src/base folder

The base folder contains everything that would be reusable between Arkanoid and other games like Pong, Tetris or Space Invaders.

### Graphics.ts

Graphics is a simple 2D renderer with an API similar to the HTML Canvas API.

Usage example:
```typescript
(async function() {
	const canvas = document.getElementById("canvas") as HTMLCanvasElement;
	const gfx = new Graphics(canvas);
	const image = await gfx.loadTexture("images/paddle.png");

	// Resize the canvas to 640 x 480 pixels;
	gfx.resizeViewport(640, 480);

	// Set the render transform to fit the canvas
	gfx.resetTransform();

	// Clear the canvas with orange
	gfx.clear(1, 0.5, 0);

	// Save the current render state
	gfx.save()

	// Center on the screen
	gfx.translate(gfx.width / 2, gfx.height / 2);

	// Scale 2x
	gfx.scale(2,2);

	// Rotate 30 degrees.
	gfx.rotate( 30 * Math.PI / 180 );

	// Draw the image centered in the screen but zoomed 2x and rotated 30 degrees.
	gfx.drawImage(image, -image.width*0.5, -image.height*0.5);

	// Restore the previously saved render state.
	gfx.restore();

	// Because the state was restored the image will now render on the top left corner.
	gfx.drawImage(image, 0, 0);
})();
```

### Input.ts

Input is a class that manages key bindings, the state of keys and the state bound actions.

- It keeps track of pressed keys so it's possible to query the current state of a key at any time without the need of registering a callback.
- It keeps track of bound **actions** and allows querying their state without callbacks but also allows registering to onChange events.

Usage example:

```typescript
const input = new Input<"action1" | "action2">()

// Bind KeyA to action1 
input.bind("KeyA", "action1");

// Also bind Enter to action1, both keys will now drive action1
input.bind("Enter", "action1");

// Bind KeyF to action2
input.bind("KeyF", "action2");

// Start listening for keyboard events
input.start();

// Listen to changes of the action2 pressed state
// onChange returns a function that can be used to disconnect the event listener.
const disconnectListener = input.action("action2").onChange(state => console.log(`action2 state: ${state}`));

// Stop listening to action2 changes
disconnectListener();

// At any point you may query the state of a key
const isSpacePressed = input.key("Space");

// Or the state of an action
const isAction1Pressed = input.action("action1").state;

// Stop listening for keyboard events, it is important to call this to avoid leaking.
input.stop()
```

### Coroutines.ts

The Unity3D game engine has a pretty useful feature called **coroutines** which are implemented with Generators. They are a really clean way of describing certain kinds of **state machine**.

In this project I experimented with using javascript generator functions to implement the same concept as Unity3D coroutines.

Coroutines.ts defines a module called coro which has a few helper coroutines in it.

Usage example:
```typescript
let x = 0;

function* myCoroutine(dt: coro.DeltaTime) {
	x = 5;

	// Wait for a key to be pressed.
	yield* coro.waitForAnyKey()

	// Animate x, start at 5 and go to 0 during the next 3 seconds.
	yield* coro.lerp(dt, 3, 5, 0, val => x = val)

	// Wait 1 second
	yield* coro.waitForSeconds(dt, 1);

	// Subroutine that animates x from 0 to 5, then prints "up", then
	// from 5 to 0, then prints "down" and repeats indefinitely
	function* subCoroutine() {
		while( true ) {
			yield* coro.lerp(dt, 1, 0, 5, val => x = val);
			console.log("up");
			yield* coro.lerp(dt, 1, 5, 0, val => x = val);
			console.log("down");
		}
	}

	// Run both subCoroutine and waitForAnyKey in parallel, stop when one of them ends.
	// Since subCoroutine will never end, this will run the subCoroutine animation
	// until a key is pressed.
	yield* coro.race(coro.waitForAnyKey(), subCoroutine())
}

let deltaTime = {value: 0};
let fsm = myCoroutine(deltaTime);

function update(dt: number) {
	deltaTime.value = dt;
	fsm.next();
}
```

A note about DeltaTime references:

In Unity3D coroutines have access to the global delta time that unity controls.

I'm not a big fan of globals so instead I'm using {value: number} objects to be able to pass delta time by reference instead of by value.

### Collision.ts

The collider folders contains classes that represent shapes that can collide with each other.

Collision.ts contains functions to check for collision between two colliders.

All collision check functions also return a normal vector pointing in the direction of minimal
separation and a penetrationDistance which is the distance the objects have to move along the
normal direction in order to fully separate.

Example usage
```typescript
const disc = new Disc(0,0,10);
const box = new Box(5,5,5,7);
const collision = new CollisionResult();

if ( checkDiscBoxCollision(disc, box, collision) ) {
	// disc.position += collision.normal * collision.penetration
	// This moves the disc away from the box so that it is no longer overlapping.
	vec2.scaleAndAdd(disc.position, disc.position, collision.normal, collision.penetrationDistance);
}
```

## src/arkanoid folder

The src/arkanoid folder contains all the non reusable logic specific to arkanoid gameplay.

The Arkanoid class is the main controller, most of its state machine logic is implemented using coroutines.

The Gameplay class implements the core gameplay physics of the game. It is independent of any graphics, audio or input.

Images.ts and Sounds.ts are in charge of loading the sound and image assets. If you want to add new files you must add them here.

## src/pong folder

Since arkanoid is a bit complex, pong provides a simpler project to use as scaffolding.

The structure inside is the same as for arkanoid.

You can play it [here](https://basro.gitlab.io/arkanoid/pong). (Up and Down Arrow keys for player 1, W and S for player 2)
