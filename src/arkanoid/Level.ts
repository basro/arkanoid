
export class Level {
	bricks: (number | null)[][] = []

	static createFromString(str: string): Level {
		const level =  new Level();
		const lines = str.split("\n");
		lines.splice(0,1); // Ignore first line

		const bricks = [];
		for ( let line of lines ) {
			const brickRow = [];
			for( let char of line ) {
				if ( char == " " || char == "_" ) {
					brickRow.push(null);
					continue;
				}

				const num = parseInt(char);
				if ( isNaN(num) ) {
					throw new Error("Invalid brick " + char);
				}
				brickRow.push(num);
			}
			bricks.push(brickRow);
		}

		level.bricks = bricks;
		return level;
	}
}

const level1 = `

1111  1111
1111  1111
2222  2222
8888  8888
1441  1441
1441  1441
2222992222
`

const level2 = `
1111111111
9222222229
_93333339
__944449
___9559
____99
6667777666


_00    00
_00    00
_00    00
_00    00
`

const level3 = `

     33
     33
    3333
    3333
    3333
    3333
    300000
    300000
    33333
    33333
  33333
  33333
 333333
 333333
 33 33
 33 33
`

export const Levels = [level1, level2, level3].map(str=>Level.createFromString(str));

