import { Images, loadImages } from './Images'
import { Sounds, loadSounds } from './Sounds'
import { Graphics } from '../base/Graphics';
import { Gameplay } from './Gameplay';
import { renderGameplay } from './GameplayRenderer';
import { playSound } from '../base/Audio';
import { coro } from '../base/Coroutines';
import { Levels, Level } from './Level';
import { Input } from '../base/Input';

export type ArkanoidActions = "left" | "right";

export class Arkanoid {
	private fsm: Generator;
	private dt: coro.DeltaTime = {value: 0};
	private renderFunc = function() {};
	private input = new Input<ArkanoidActions>();

	constructor( private gfx: Graphics, private audio: AudioContext, public images: Images, public sounds: Sounds ) {
		this.fsm = this.mainRoutine();

		this.input.bind("ArrowLeft", "left");
		this.input.bind("KeyA", "left");
		this.input.bind("ArrowRight", "right");
		this.input.bind("KeyD", "right");
	}

	static async create(gfx: Graphics, audio: AudioContext): Promise<Arkanoid> {
		const [images, sounds] = await Promise.all([loadImages(gfx), loadSounds(audio)]);
		return new Arkanoid(gfx, audio, images, sounds);
	}

	update(dt: number) {
		this.dt.value = dt;
		this.fsm.next();
	}

	render() {
		const gfx = this.gfx;
		gfx.resetTransform();
		gfx.clear(0,0,0);
		gfx.translate(gfx.width >> 1, 0);
		const scale = Math.min(gfx.width / 320, gfx.height / 260);
		gfx.scale(scale, scale);

		this.gfx.save();
		this.renderFunc();
		this.gfx.restore();
	}

	private *mainMenuRoutine() {
		const {dt, gfx, images} = this;
		
		let logoX = 0;
		let logoY = 0;
		let showText = false;
		let text = "";
		this.renderFunc = () => {
			gfx.drawImage(images.logo, logoX - (images.logo.width >> 1), logoY);
			if ( showText ) gfx.drawTextCentered(images.font, text, 0, 150);
		}

		yield* coro.lerp(dt, 1.5, -images.logo.height - 30, 80, value => logoY = value);

		function* animateText() {
			yield* coro.waitForSeconds(dt, 1);
			showText = true;
			const message = "press any key";
			for ( let i = 0; i <= message.length; ++i ) {
				text = message.substr(0,i);
				yield* coro.waitForSeconds(dt, 0.025);
			}

			while( true ) {
				yield* coro.waitForSeconds(dt, 0.5);
				showText = false;
				yield* coro.waitForSeconds(dt, 0.5);
				showText = true;
			}
		}

		yield* coro.race(coro.waitForAnyKey(), animateText());
	}

	private *gameOverRoutine() {
		const {dt, gfx, images} = this;

		let scaleX = 1;
		let scaleY = 1;
		this.renderFunc = () => {
			gfx.translate(0, 100);
			gfx.scale(scaleX, scaleY);
			gfx.translate(-images.gameover.width >> 1, -images.gameover.height >> 1);
			gfx.drawImage(images.gameover, 0,0);
		}

		yield* coro.lerp(dt, 1.5, 0, 1, factor => {
			scaleX = Math.cos(Math.PI * 4 * factor) * factor;
			scaleY = factor;
		});

		yield* coro.waitForAnyKey();
	}

	private *gameRoutine() {
		const {dt, gfx, audio, input, sounds, images} = this;

		const game = new Gameplay();
		let level = 0;
		this.renderFunc = () => {
			gfx.translate(-40,0);
			gfx.drawText(images.font, `score ${game.score}`, 100, 30);
			gfx.drawText(images.font, `lives ${game.lives}`, 100, 50);
			renderGameplay(gfx, game, images);
		}

		game.onBallBounce = (kind) => {
			playSound(audio, [sounds.bounce1, sounds.bounce2, sounds.bounce3][kind]);
		}

		const cleanup = [
			input.action("left").onChange(value => game.playerInput.left = value),
			input.action("right").onChange(value => game.playerInput.right = value)
		];

		function loadLevel() {
			game.reset(Levels[level%Levels.length]);
		}

		// Register developer console cheats:
		(window as any).arkanoid = {
			infiniteLives: () => game.lives = 9999,
			win: () => game.state = "win",
			loadLevel: (level:string) => game.reset(Level.createFromString(level))
		}
		
		loadLevel();
		while ( game.lives > 0 ) {
			playSound(audio, sounds.start);
			game.resetPaddleAndBall();

			{
				let t = 2;
				while ( t > 0 ) {
					t -= dt.value;
					game.update(dt.value);
					game.resetBall();
					yield;
				}
			}

			playSound(audio, sounds.bounce1);

			while( game.state == "playing" ) {
				game.update(dt.value);
				yield;
			}

			if ( game.state == "lose" ) {
				playSound(audio, sounds.death);
				game.lives--;
			}

			yield* coro.waitForSeconds(dt,1);

			if ( game.state == "win" ) {
				level++;
				loadLevel();
			}
		}

		cleanup.forEach(cleaner => cleaner());
	}

	private *mainRoutine() {
		this.input.start();
		while (true) {
			yield* this.mainMenuRoutine();
			yield* this.gameRoutine();
			yield* this.gameOverRoutine();
		}
	}
}
