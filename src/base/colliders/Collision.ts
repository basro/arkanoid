import Box from './Box';
import Disc from './Disc';
import { vec2 } from 'gl-matrix';
import { vec2Sign } from '../Math';

/**
 * Class representing the results of a collision check.
 */
export class CollisionResult {
	/** Normal vector in the direction of shortest separation. */
	normal = vec2.create();

	/** Distance along the normal needed to fully separate the colliding objects. */
	penetrationDistance = 0;
}

const diff = vec2.create();
const dist = vec2.create();
const sign = vec2.create();

/**
 * Checks if a disc and a box collide.
 * 
 * @param disc a disc collider to test for collision.
 * @param box a box collider to test for collision.
 * @param outResults CollisionResult reference were results will be stored. The normal will point towards disc.
 * 
 * @returns true if the disc and box are colliding, otherwise false.
 */
export function checkDiscBoxCollision(disc: Disc, box: Box, outResults: CollisionResult ): boolean {
	const dpos = disc.position;
	const bpos = box.position;

	vec2.sub(diff, dpos, bpos); // diff = bpos - dpos;
	vec2Sign(sign, diff); // sign = sign(diff)
	vec2.mul(diff, diff, sign); // diff = abs(diff)
	vec2.sub(dist, diff, box.halfSize); // dist = diff - box.halfSize;

	let pointDistance = 0;
	const normal = outResults.normal;
	if ( dist[0] > 0 && dist[1] > 0 ) {
		pointDistance = vec2.length(dist);
		vec2.scale(normal, dist, 1/pointDistance);
	}else if( dist[0] > dist[1] ) {
		pointDistance = dist[0];
		vec2.set(normal, 1, 0);
	}else {
		pointDistance = dist[1];
		vec2.set(normal, 0, 1);
	}

	vec2.mul(normal, normal, sign);	

	outResults.penetrationDistance = disc.radius - pointDistance
	return outResults.penetrationDistance > 0;
}


/**
 * Checks if two boxes collide.
 * 
 * @param A a box to test for collision.
 * @param B a box to test for collision.
 * @param outResults CollisionResult reference were results will be stored. The normal will point towards A.
 * 
 * @returns true if the boxes collide, otherwise false.
 */
export function checkBoxBoxCollision(A: Box, B: Box, outResults: CollisionResult): boolean {
	const {position: Apos, halfSize: Asize} = A;
	const {position: Bpos, halfSize: Bsize} = B;

	vec2.sub(diff, Apos, Bpos);  // diff = Apos - Bpos
	vec2Sign(sign, diff);        // sign = sign(diff)
	vec2.mul(diff, diff, sign);  // diff = abs(diff)
	vec2.sub(dist, diff, Asize);
	vec2.sub(dist, dist, Bsize); // dist = diff - Bsize - Asize;

	const normal = outResults.normal;
	if( dist[0] > dist[1] ) {
		outResults.penetrationDistance = -dist[0];
		vec2.set(normal, 1, 0);
	}else {
		outResults.penetrationDistance = -dist[1];
		vec2.set(normal, 0, 1);
	}

	vec2.mul(normal, normal, sign);
	return outResults.penetrationDistance > 0;
}