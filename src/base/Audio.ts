/**
 * Plays a sound.
 * 
 * @param audio the audio context in which to play the sound.
 * @param sound the AudioBuffer to play.
 */
export function playSound(audio: AudioContext, sound: AudioBuffer) {
	const source = audio.createBufferSource();
	source.buffer = sound;
	source.connect(audio.destination);
	source.start();
}

/**
 * Fetches a sound from a url.
 * 
 * @param ctx the audio context to decode the sound.
 * @param url the url from which to fetch the sound.
 * 
 * @returns A promise of an audio buffer.
 */
export async function loadSound(ctx: AudioContext, url: string): Promise<AudioBuffer> {
	const fileData = await (await fetch(url)).arrayBuffer();
	return await ctx.decodeAudioData(fileData)
}
