import Box from '../base/colliders/Box';
import { vec2 } from 'gl-matrix';

export default class Brick {
	collider: Box;
	hits: number;
	type: number;
	lastHitTime: number = 0;

	constructor(x: number, y: number, type: number) {
		this.collider = new Box(x,y,16,8);
		this.hits = type >= 8? 2 : 1;
		this.type = type;
	}
}