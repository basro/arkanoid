import { Gameplay } from './Gameplay';
import { vec2 } from 'gl-matrix';
import Disc from '../base/colliders/Disc';
import { CollisionResult, checkDiscBoxCollision } from '../base/colliders/Collision';

const collision = new CollisionResult();

export default class Ball {
	collider: Disc = new Disc(0,0,5);
	vel = vec2.create();

	update(dt: number, game: Gameplay) {
		const {position: pos, radius} = this.collider;
		const vel = this.vel;
		vec2.scaleAndAdd(pos, pos, vel, dt);

		let bounced = false;

		// Collide with playArea
		const playArea = game.playArea;
		if ( vel[1] < 0 ) {
			if ( pos[1] - radius < playArea.top ) {
				vel[1] *= -1;
				bounced = true;
			}
		}else {
			if ( pos[1] + radius > playArea.bottom ) {
				vel[1] *= -1;
				bounced = true;
			}
		}

		// Collide with paddles
		for ( let paddle of game.paddles ) {
			if ( checkDiscBoxCollision(this.collider, paddle, collision) ) {
				this.respondToCollision(collision);
				bounced = true;
			}
		}

		if ( bounced && game.onBallBounce ) game.onBallBounce();
	}

	respondToCollision( collision: CollisionResult ) {
		const pos = this.collider.position;
		const vel = this.vel;
		const normal = collision.normal;
		vec2.scaleAndAdd(pos, pos, normal, collision.penetrationDistance);
		const nvel = vec2.dot(normal, vel);
		if ( nvel < 0 ) {
			vec2.scaleAndAdd(vel, vel, normal, -2 * nvel * 1.02);
		}
	}
}