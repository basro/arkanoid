import { Graphics } from './base/Graphics';
import { Arkanoid } from './arkanoid/Arkanoid';
import { runMainLoop } from './base/MainLoop';

async function init() {
	const canvas = document.getElementById("canvas") as HTMLCanvasElement;
	const gfx = new Graphics(canvas);
	const audio = new AudioContext();
	const arkanoid = await Arkanoid.create(gfx, audio);

	runMainLoop(
		(dt) => arkanoid.update(dt),
		() => {
			gfx.resizeViewport();
			arkanoid.render();
		},
		1/120
	);
}
init();
