

/**
 * Modify an array in place by removing elements depending on the result of a callback function.
 * 
 * @param array the array to modify.
 * @param condition the filter condition. Elements for which this function returns falsy will be removed.
 */
export function filterInPlace<T>(array: Array<T>, condition: (value: T) => boolean)
{
	let index = 0;
	for (let value of array)
	{
		if (condition(value)) {
			array[index++] = value;
		}
	}
	array.splice(index);
}
