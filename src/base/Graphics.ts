import defaultShaderVert from './shaders/shader.vert.glsl';
import defaultShaderFrag from './shaders/shader.frag.glsl';
import { mat4, mat3, vec2, vec4 } from 'gl-matrix';

const MAX_RENDER_STACK_SIZE = 256;

/**
 * A simple WebGL renderer with an interface similar in use to CanvasRenderingContext2D.
 */
export class Graphics {
	private gl: WebGLRenderingContext;
	private defaultShader: Shader;
	private quadVertexBuffer: WebGLBuffer;
	private quadVertexData: Float32Array;
	private whiteTexture: Texture;

	private renderState: RenderState = new RenderState();
	private renderStateStack: RenderState[] = [];

	/**
	 * Create a new Graphics instance using a canvas element as backing for the WebGL context.
	 * 
	 * @param canvas the backing canvas to use for the WebGL context.
	 */
	constructor(canvas: HTMLCanvasElement) {
		let gl = canvas.getContext("webgl", { alpha: false, depth: false });
		if ( gl == null ) {
			throw Error("Unable to create WebGLRenderingContext");
		}
		this.gl = gl;

		const shaderProgram = initShaderProgram(gl, defaultShaderVert, defaultShaderFrag);
		this.defaultShader = {
			program: shaderProgram,
			locations: {
				aVertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
				aTextureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
				uTransform: gl.getUniformLocation(shaderProgram, 'uTransform'),
				uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
				uTint: gl.getUniformLocation(shaderProgram, 'uTint'),
			},
		};

		this.quadVertexBuffer = gl.createBuffer()!;
		gl.bindBuffer(gl.ARRAY_BUFFER, this.quadVertexBuffer);

		this.quadVertexData = new Float32Array(4*4);
		updateQuadVertexData(this.quadVertexData, 0, 0, 0, 0, 1, 1, 1, 1);

		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(this.quadVertexData), gl.DYNAMIC_DRAW);

		this.whiteTexture = makeWhiteTexture(gl);
	}

	/** The width of the viewport. */
	get width() {
		return this.gl.canvas.width;
	}


	/** The height of the viewport. */
	get height() {
		return this.gl.canvas.height;
	}

	/**
	 * Resizes the viewport match the backing canvas element computed css dimensions.
	 * The device's pixel ratio is taken into account so the resulting viewport size will match the true screen pixel dimensions of the canvas.
	 */
	resizeViewport(): void;

	/**
	 * Resizes the viewport to the specified dimensions.
	 * This modifies the backing canvas width and height.
	 *
	 * @param width the target viewport width.
	 * @param height the target viewport height.
	 */
	resizeViewport(width: number, height: number): void;
	resizeViewport(width?: number, height?: number): void {
		const gl = this.gl;
		const canvas = gl.canvas as HTMLCanvasElement;
		if ( width == undefined ) {
			const rect = canvas.getBoundingClientRect();
			const pixelRatio = window.devicePixelRatio;
			width = Math.round(rect.width * pixelRatio);
			height = Math.round(rect.height * pixelRatio);
		}
		
		if ( canvas.width != width || canvas.height != height ) {
			canvas.width = width;
			canvas.height = height!;
		}
		gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
	}

	/**
	 * Clears the canvas with the specified color.
	 * 
	 * @param r the red component.
	 * @param g the green component.
	 * @param b the blue component.
	 */
	clear(r: number, g: number, b: number) {
		const gl = this.gl;
		gl.clearColor(r,g,b,0);
		gl.clear(gl.COLOR_BUFFER_BIT);
	}

	/**
	 * Resets the transform state.
	 * Coordinates (0,0) will match the top left of the screen.
	 * Coordinates (width, height) will match the bottom right of the screen.
	 */
	resetTransform() {
		const gl = this.gl;
		const transform = this.renderState.transform;
		mat3.identity(transform);
		mat3.scale(transform, transform, [2/gl.canvas.width,-2/gl.canvas.height]);
		mat3.translate(transform, transform, [-gl.canvas.width >> 1,-gl.canvas.height >> 1]);
	}

	/**
	 * Applies a translation to the transform state.
	 * 
	 * @param x the amount of translation in the x axis.
	 * @param x the amount of translation in the y axis.
	 */
	translate(x: number, y: number) {
		const transform = this.renderState.transform;
		mat3.translate(transform, transform, [x,y]);
	}

	/**
	 * Applies a rotation to the transform state.
	 * 
	 * @param radians the angle of rotation in radians.
	 */
	rotate(radians: number) {
		const transform = this.renderState.transform;
		mat3.rotate(transform, transform, radians);
	}

	/**
	 * Applies a scale transformation to the transform state
	 * 
	 * @param x the amount of scaling on the x axis.
	 * @param y the amount of scaling on the y axis.
	 */
	scale(x: number, y: number) {
		const transform = this.renderState.transform;
		mat3.scale(transform, transform, [x,y]);
	}

	/**
	 * Sets the tint color.
	 * All draw operations will have their colors multiplied by the tint color.
	 * 
	 * @param r the red component.
	 * @param g the green component.
	 * @param b the blue component.
	 * @param a the alpha component.
	 */
	tint(r: number, g: number, b: number, a: number) {
		vec4.set(this.renderState.tint, r, g, b, a);
	}

	/**
	 * Saves the current rendering state by pushing into a stack.
	 * 
	 * To restore the saved state you must call restore().
	 * There must be one call to restore for every call to save() or the stack will eventually overflow.
	 */
	save() {
		if (this.renderStateStack.length > MAX_RENDER_STACK_SIZE) {
			throw new Error("Render state stack overflow");
		}
		this.renderStateStack.push(new RenderState().copy(this.renderState));
	}

	/**
	 * Restores the rendering state to the values use in the last call to save() and pops the saved states stack.
	 */
	restore() {
		const state = this.renderState;
		const newState = this.renderStateStack.pop();
		if ( newState == null ) {
			throw new Error("Render state stack underflow");
		}
		state.copy(newState);
	}

	/**
	 * Draws a fragment of an image onto the screen.
	 * 
	 * @param texture the image source to draw from.
	 * @param x the desination x coordinates.
	 * @param y the destination y coordinates.
	 * @param sx the source rectangle x coordinates.
	 * @param sy the source rectangle y coordinates.
	 * @param swidth the source rectangle width.
	 * @param sheight the source rectangle height.
	 */
	drawSubImage(texture: Texture, x: number, y: number, sx: number, sy: number, swidth: number, sheight: number) {
		const u0 = sx / texture.width;
		const v0 = sy / texture.height;
		const u1 = (sx + swidth) / texture.width;
		const v1 = (sy + sheight) / texture.height;
		updateQuadVertexData(this.quadVertexData, x,y,u0,v0,x+swidth,y+sheight,u1,v1);
		this.drawImageHelper(texture);
	}

	/**
	 * Draws an image onto the screen.
	 * 
	 * If width or height are null or undefined the texture dimensions will be used instead.
	 * 
	 * @param texture the image to draw.
	 * @param x the destination rectangle x coordinates.
	 * @param y the destination rectangle y coordinates.
	 * @param width the destination rectangle width.
	 * @param height the destination rectangle height.
	 */
	drawImage(texture: Texture, x: number, y: number, width?: number, height?: number) {
		if ( width == null ) {
			width = texture.width;
		}
		if ( height == null ) {
			height = texture.height;
		}
		updateQuadVertexData(this.quadVertexData, x,y,0,0,x+width,y+height,1,1);
		this.drawImageHelper(texture);
	}

	private drawImageHelper(texture: Texture) {
		const gl = this.gl;
		gl.bindBuffer(gl.ARRAY_BUFFER, this.quadVertexBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, this.quadVertexData, gl.DYNAMIC_DRAW);

		const locations = this.defaultShader.locations;
		gl.vertexAttribPointer(locations.aVertexPosition, 2, gl.FLOAT, false, 4*4,0);
		gl.enableVertexAttribArray(locations.aVertexPosition);
		gl.vertexAttribPointer(locations.aTextureCoord, 2, gl.FLOAT, false, 4*4,2*4);
		gl.enableVertexAttribArray(locations.aTextureCoord);

		gl.useProgram(this.defaultShader.program);
		gl.uniformMatrix3fv(locations.uTransform, false, this.renderState.transform);
		gl.uniform1i(locations.uSampler, 0);
		gl.uniform4fv(locations.uTint, this.renderState.tint);
		
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, texture.handle);
		gl.enable(gl.BLEND);
		gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
		
		gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	}

	/**
	 * Draws a white rectangle onto the screen.
	 * Combine with tint to draw a rectangle of any color.
	 * 
	 * @param x the rect x coordinate.
	 * @param y the rect y coordinate.
	 * @param width the rect width.
	 * @param height the rect height.
	 */
	drawRect(x:number, y: number, width: number, height: number) {
		this.drawImage(this.whiteTexture, x, y, width, height);
	}

	/**
	 * Draws text onto the screen.
	 * 
	 * @param font the font to use.
	 * @param text the string to draw.
	 * @param x the left position of the text bounding box.
	 * @param y the top position of the text bounding box.
	 * @param spacing the space between each character, defaults to 1.
	 */
	drawText(font: Font, text: string, x: number, y: number, spacing?: number) {
		if ( spacing == undefined ) spacing = 1;

		for ( let char of text ) {
			const charPos = font.charMap.get(char);
			if ( charPos != null ) {
				this.drawSubImage(font.texture, x, y, charPos.x, charPos.y, font.charWidth, font.charHeight);
			}
			x += font.charWidth + spacing;
		}
	}

	/**
	 * Draws text onto the screen, centering it along the x axis.
	 * 
	 * @param font the font to use.
	 * @param text the string to draw.
	 * @param x the horizontal center position of the text bounding box.
	 * @param y the top position of the text bounding box.
	 * @param spacing the space between each character, defaults to 1.
	 */
	drawTextCentered(font: Font, text: string, x: number, y: number, spacing?: number) {
		if ( spacing == undefined ) spacing = 1;
		const width = (font.charWidth + spacing) * text.length;
		this.drawText(font, text, x - width * 0.5, y, spacing);
	}

	/**
	 * Fetches a texture from a url
	 * 
	 * @param url the url from which to fetch the texture
	 * @param options texture options
	 * 
	 * @returns a promise of the fetched texture
	 */
	async loadTexture(url: string, options?: {repeat?: boolean}): Promise<Texture> {
		const gl = this.gl;
		const image = await loadImage(url);
		const texture = gl.createTexture()!;
		gl.bindTexture(gl.TEXTURE_2D, texture);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
	
		let wrap = gl.CLAMP_TO_EDGE;
		if ( options && options.repeat ) wrap = gl.REPEAT;

		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, wrap);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, wrap);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
	
		return {handle: texture, width: image.width, height: image.height};
	}

	/**
	 * Fetches a bitmap font from a url.
	 * 
	 * @param url the url from which to fetch the font texture.
	 * @param charWidth the width of a single character in the font.
	 * @param charHeight the height of a single character in the font.
	 * @param padding the separation in pixels between each character in the font.
	 * @param characters a string of characters that appear in the font. They must match the order from left to right, top to bottom.
	 * 
	 * @returns a promise of the fetched font.
	 */
	async loadFont(url: string, charWidth: number, charHeight: number, padding: number, characters: string): Promise<Font> {
		const texture = await this.loadTexture(url);
		const charMap = new Map<string, {x: number, y:number}>();
		let x = 0;
		let y = 0;
		for (let char of characters) {
			charMap.set(char, {x,y});
			x += charWidth + padding;
			if ( x >= texture.width ) {
				x = 0;
				y += charHeight + padding;
			}
		}

		return {texture, charMap, charWidth, charHeight}
	}
}

function initShaderProgram(gl: WebGLRenderingContext, vertSource: string, fragSource: string) {
	const vertShader = loadShader(gl, gl.VERTEX_SHADER, vertSource);
	const fragShader = loadShader(gl, gl.FRAGMENT_SHADER, fragSource);

	const shaderProgram = gl.createProgram()!;
	gl.attachShader(shaderProgram, vertShader);
	gl.attachShader(shaderProgram, fragShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		throw Error('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
	}

	return shaderProgram;
}

function loadShader(gl: WebGLRenderingContext, type: number, source: string): WebGLShader {
	const shader = gl.createShader(type)!;
	gl.shaderSource(shader, source);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		throw Error('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
	}

	return shader;
}

/**
 * An image that can be drawn by Graphics draw functions
 */
export interface Texture {
	/** The WebGL texture handle. */
	handle: WebGLTexture;

	/** The texture width in pixels. */
	width: number;

	/** The texture height in pixels. */
	height: number;
}

/**
 * A bitmap font that can be drawn by Graphics drawText functions.
 */
export interface Font {
	/** The font bitmap. */
	texture: Texture;

	/** The width of a single character in the font. */
	charWidth: number;

	/** The height of a single character in the font. */
	charHeight: number;

	/** Maps characters to their respective coordinates in the font texture. */
	charMap: Map<string, {x: number, y:number}>;
}

function makeWhiteTexture(gl: WebGLRenderingContext) {
	const texture = gl.createTexture()!;
	gl.bindTexture(gl.TEXTURE_2D, texture);
	gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, 32, 32, 0, gl.LUMINANCE, gl.UNSIGNED_BYTE, new Uint8Array(32*32).fill(255));
	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
	return {handle: texture, width: 32, height: 32};
}

function loadImage(url: string): Promise<HTMLImageElement> {
	return new Promise((resolve, reject) => {
		const image = new Image();
		image.onload = () => resolve(image);
		image.onerror = (e) => reject(e);
		image.src = url;
	});
}

function updateQuadVertexData(vd: Float32Array, x0: number, y0: number, u0: number, v0: number, x1: number, y1: number, u1: number, v1: number) {
	// v0
	vd[0] = x0;
	vd[1] = y0;
	vd[2] = u0;
	vd[3] = v0;
	// v1
	vd[4] = x1;
	vd[5] = y0;
	vd[6] = u1;
	vd[7] = v0;
	// v2
	vd[8] = x1;
	vd[9] = y1;
	vd[10] = u1;
	vd[11] = v1;
	// v3
	vd[12] = x0;
	vd[13] = y1;
	vd[14] = u0;
	vd[15] = v1;
}

interface Shader {
	program: WebGLProgram;
	locations: {
		aVertexPosition: number;
		aTextureCoord: number;
		uTransform: WebGLUniformLocation | null;
		uSampler: WebGLUniformLocation | null;
		uTint: WebGLUniformLocation | null;
	}
}

class RenderState {
	transform: mat3 = mat3.create();
	tint: vec4 = vec4.fromValues(1,1,1,1);

	copy(source: RenderState) {
		mat3.copy(this.transform, source.transform);
		vec4.copy(this.tint, source.tint);
		return this;
	}
}