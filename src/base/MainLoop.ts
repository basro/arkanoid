
/**
 * Sets up a main loop that will run update(dt) and render() repeatedly.
 * 
 * - Automatically computes delta time.
 * - Skips frames if the delta time is too big.
 * - Splits delta time into smaller chunks and calls update repeatedly to ensure consistent physics.
 * 
 * @param updateFunc callback that handles game logic update, gets dela time as argument.
 * @param renderFunc callback that handles rendering logic. This can be called less frequently that updateFunc.
 * @param maxDeltaTime maximum value of delta time that updateFunc should recieve. When delta time is bigger
 * than this value updateFunc will be called multiple times in order to catch up without exceeding it.
 */
export function runMainLoop(updateFunc: (dt: number) => void, renderFunc: () => void, maxDeltaTime: number) {
	let lastTimer = performance.now();

	function handleAnimationFrame(time: number) {
		let dt = Math.min((time - lastTimer) / 1000, 0.2);
		lastTimer = time;
	
		while ( dt > 0 ) {
			const subDT = Math.min(dt, maxDeltaTime);
			updateFunc(subDT);
			dt -= subDT;
		}
		
		renderFunc();
		requestAnimationFrame(handleAnimationFrame);
	}
	
	requestAnimationFrame(handleAnimationFrame);
}

