varying highp vec2 vTextureCoord;

uniform sampler2D uSampler;
uniform lowp vec4 uTint;

void main(void) {
	gl_FragColor = texture2D(uSampler, vTextureCoord) * uTint;
}