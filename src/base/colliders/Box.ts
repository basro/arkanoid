import { vec2 } from 'gl-matrix';

/**
 * A collider representing a 2D Box centered on a point.
 */
export default class Box {
	/** The center of the box. */
	position = vec2.create();

	/** A vector representing half the width and height of the box. */
	halfSize = vec2.create();

	constructor(x: number, y: number, width: number, height: number) {
		vec2.set(this.position, x,y);
		vec2.set(this.halfSize, width*0.5, height*0.5);
	}

	/** The full width of the box */
	get width() {
		return this.halfSize[0]*2;
	}

	/** The full height of the box */
	get height() {
		return this.halfSize[1]*2;
	}

	/** The X coordinate of the left side of the box. */
	get left() {
		return this.position[0] - this.halfSize[0];
	}

	/** The X coordinate of the right side of the box. */
	get right() {
		return this.position[0] + this.halfSize[0];
	}

	/** The Y coordinate of the top side of the box. */
	get top() {
		return this.position[1] - this.halfSize[1];
	}

	/** The Y coordinate of the bottom side of the box. */
	get bottom() {
		return this.position[1] + this.halfSize[1];
	}
}