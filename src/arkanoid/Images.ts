import { Texture, Graphics, Font } from '../base/Graphics';

export interface Images {
	paddleTex: Texture;
	bricksTex: Texture;
	ballTex: Texture;
	backgroundTex: Texture;
	backgroundBorderTex: Texture;
	logo: Texture;
	gameover: Texture;
	font: Font;
}

export async function loadImages(gfx: Graphics): Promise<Images> {
	const [paddleTex, bricksTex, ballTex, backgroundBorderTex, logo, gameover, white] = await Promise.all([
			"paddle.png",
			"bricks.png",
			"ball.png",
			"bg-border.png",
			"logo.png",
			"gameover.png",
		].map(name => gfx.loadTexture("images/"+name))
	);
	const backgroundTex = await gfx.loadTexture("images/background.png", {repeat: true});
	const font = await gfx.loadFont("images/font.png", 8,8,1,"abcdefghijklmnopqrstuvwxyz0123456789.<>-O!?_");
	return {paddleTex, bricksTex, ballTex, backgroundBorderTex, backgroundTex, logo, gameover, font};
}
