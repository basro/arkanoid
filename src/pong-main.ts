import { Graphics } from './base/Graphics';
import { runMainLoop } from './base/MainLoop';
import { Pong } from './pong/Pong';

async function init() {
	const canvas = document.getElementById("canvas") as HTMLCanvasElement;
	const gfx = new Graphics(canvas);
	const audio = new AudioContext();
	const game = await Pong.create(gfx, audio);

	runMainLoop(
		(dt) => game.update(dt),
		() => {
			gfx.resizeViewport();
			game.render();
		},
		1/120
	);
}
init();
