import { Texture, Graphics } from '../base/Graphics';
import { Gameplay } from './Gameplay';
import Brick from './Brick';
import { Images } from './Images';

export function renderGameplay(gfx: Graphics, state: Gameplay, images: Images) {
	const {paddle, ball} = state;

	function drawBrick(brick: Brick, isShadows: boolean) {
		const brickWidth = images.bricksTex.width / 10;
		const height = images.bricksTex.height;
		gfx.drawSubImage(images.bricksTex, brick.collider.left, brick.collider.top, brick.type * brickWidth, 0, brickWidth - 1, height);

		if ( isShadows ) return;
		const timeSinceHit = performance.now() - brick.lastHitTime;
		const duration = 100;
		const factor = 1 - (timeSinceHit / duration);
		if ( factor > 0 ) {
			gfx.save();
			gfx.tint(1,1,1, factor * 0.7);
			gfx.drawRect(brick.collider.left, brick.collider.top, brick.collider.width-1, brick.collider.height-1);
			gfx.restore();
		}
	}
	
	function drawBricks(isShadows: boolean) {
		for ( let brick of state.bricks ) {
			drawBrick(brick, isShadows);
		}
	}
	
	function drawForegroundItems(isShadows: boolean) {
		drawBricks(isShadows);

		gfx.drawImage(images.backgroundBorderTex, 0, 0);
		const paddlePos = paddle.collider.position;
		const ballPos = ball.collider.position;
		drawCentered(gfx, images.paddleTex, paddlePos[0], paddlePos[1]);
		drawCentered(gfx, images.ballTex, ballPos[0], ballPos[1]);
	}

	gfx.translate(-images.backgroundBorderTex.width >> 1, 20);
	gfx.drawSubImage(images.backgroundTex, 0, 0, 0, 0, images.backgroundBorderTex.width, images.backgroundBorderTex.height);

	gfx.save();
	gfx.tint(0,0,0,0.5);
	gfx.translate(5,5);
	drawForegroundItems(true);
	gfx.restore();

	drawForegroundItems(false);
}

function drawCentered(gfx: Graphics, texture: Texture, x: number, y: number) {
	gfx.drawImage(texture, x - texture.width * 0.5, y - texture.height * 0.5);
}