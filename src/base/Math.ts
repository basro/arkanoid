import { vec2 } from 'gl-matrix';

/**
 * Linearly interpolates between a start and end value.
 * 
 * @param startValue the value to output when factor is 0.
 * @param endValue the value to output when factor is 1.
 * @param factor the interpolation factor.
 * 
 * @return the interpolated value.
 */
export function lerp(startValue: number, endValue: number, factor: number) {
	return startValue + (endValue-startValue) * factor;
}

/**
 * Clamp a value between a min max.
 * 
 * @param v the value to clamp.
 * @param min the minimum value val can be.
 * @param max the maximum value val can be.
 * 
 * @return the clamped value.
 */
export function clamp(v: number, min: number, max: number) {
	if ( v < min ) return min;
	else if ( v > max ) return max;
	else return v;
}

/**
 * Apply Math.sign to every component of a vec2
 * 
 * @param outVec the vector to store the result in.
 * @param v the vector to apply sign on.
 */
export function vec2Sign(outVec: vec2, v: vec2) {
	outVec[0] = Math.sign(v[0]);
	outVec[1] = Math.sign(v[1]);
}