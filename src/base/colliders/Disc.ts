import { vec2 } from 'gl-matrix';

/**
 * A collider representing a 2D disc centered at a point.
 */
export default class Disc {
	/** The position of center of the disc */
	position = vec2.create();

	/** The radius of the disc */
	radius = 0;

	/**
	 * Create a new Disc
	 * 
	 * @param x the x coordinate of the disc center.
	 * @param y the y coordinate of the disc center.
	 * @param radius the radius of the disc.
	 */
	constructor(x: number, y: number, radius: number) {
		vec2.set(this.position, x, y);
		this.radius = radius;
	}
}