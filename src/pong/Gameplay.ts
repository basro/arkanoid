import Ball from './Ball';
import Rectangle from '../base/Rectangle';
import { vec2 } from 'gl-matrix';
import { CollisionResult, checkDiscBoxCollision } from '../base/colliders/Collision';
import { clamp } from '../base/Math';
import Box from '../base/colliders/Box';


const PADDLE_SPEED = 100;

class PlayerInput {
	up: boolean = false;
	down: boolean = false;
}

type StateName = "playing" | "p1_score" | "p2_score";

export class Gameplay {
	input = [new PlayerInput(), new PlayerInput()];
	paddles = [new Box(-150,0,10,50), new Box(150,0,10,50)];
	ball = new Ball();
	playArea = new Rectangle(-180,-100,360,200);
	state: StateName = "playing";

	onBallBounce?: () => void;

	reset(direction: number) {
		this.state = "playing";
		for ( let paddle of this.paddles ) {
			paddle.position[1] = 0;
		}

		vec2.set(this.ball.collider.position, 0,0);
		vec2.set(this.ball.vel, 100 * direction, 100);
	}

	update(dt: number) {
		if ( this.state != "playing" ) {
			return;
		}

		this.updatePaddles(dt)
		this.ball.update(dt, this);

		if ( this.ball.collider.position[0] < this.playArea.left ) {
			this.state = "p2_score";
		}else if ( this.ball.collider.position[0] > this.playArea.right ) {
			this.state = "p1_score";
		}
	}

	updatePaddles(dt: number) {
		for ( let i = 0; i < 2; ++i ) {
			const paddle = this.paddles[i];
			const input = this.input[i];

			if ( input.up ) {
				this.movePaddle(paddle, -PADDLE_SPEED * dt);
			}
			else if ( input.down ) {
				this.movePaddle(paddle, PADDLE_SPEED * dt);
			}
		}
	}

	movePaddle(paddle: Box, distance: number) {
		const paddleLen = paddle.halfSize[1];
		paddle.position[1] = clamp(paddle.position[1] + distance, this.playArea.top + paddleLen, this.playArea.bottom - paddleLen);
	}

}
