
/**
 * Helper class for keyboard events.
 * 
 * - Keeps track of the key states so you can query them without the need to use an event.
 * - Lets you bind keys to actions
 * - Keeps track of actions state, which you can query or subscribe to change events.
 * 
 * @param ActionT The type to use as action identifiers. Use this to provide additional type safety
 * by using typescript enums, for example: ("jump" | "fire" | "moveLeft" | "moveRight")
 */
export class Input<ActionT = string> {

	private actions = new Map<ActionT, Action<ActionT>>();
	private bindings = new Map<string, Action<ActionT>>();
	private pressedKeys = new Set<string>();
	private unregisterFunc: (() => void) | null = null;

	/**
	 * Starts listening keyboard events.
	 * 
	 * This will add event listeners to window.document. Make sure to call stop() to avoid memory leaks.
	 */
	start() {
		if ( this.unregisterFunc != null ) {
			return;
		}
		const {bindings} = this;

		const keyDownHandler = (event: KeyboardEvent) => this.keyChangeHandler(event.code, true);
		const keyUpHandler = (event: KeyboardEvent) => this.keyChangeHandler(event.code, false);

		document.addEventListener("keydown", keyDownHandler);
		document.addEventListener("keyup", keyUpHandler);

		this.unregisterFunc = () => {
			document.removeEventListener("keydown", keyDownHandler);
			document.removeEventListener("keyup", keyUpHandler);
		}
	}

	/**
	 * Stop listing to keyboard events.
	 */
	stop() {
		if ( this.unregisterFunc != null ) this.unregisterFunc();
	}

	/**
	 * Bind a key to an action
	 * 
	 * @param key the key to be bound.
	 * @param action the action to bind the key to.
	 */
	bind( key: string, actionName: ActionT ) {
		const action = this.getOrCreateAction(actionName);
		this.bindings.set(key, action);
	}

	/**
	 * Unbind a key
	 * 
	 * @param key the key to be unbound.
	 */
	unbind( key: string ) {
		this.bindings.delete(key);
	}

	/**
	 * Get a reference to an action.
	 * 
	 * @param actionName the action to get.
	 * 
	 * @return a reference to the action.
	 */
	action(actionName: ActionT): Action<ActionT> {
		return this.getOrCreateAction(actionName);
	}

	/**
	 * Query the state of a key.
	 * 
	 * @param key the key to get the state from.
	 * 
	 * @return a boolean representing wether the key is pressed or not.
	 */
	key(key: string) {
		return this.pressedKeys.has(key);
	}

	private getOrCreateAction(name: ActionT) {
		let action = this.actions.get(name);
		if ( action == null ) {
			action = new Action(name);
			this.actions.set(name, action);
		}
		return action;
	}

	private keyChangeHandler(key: string, value: boolean) {
		const action = this.bindings.get(key);
		if ( action != null ) {
			action.state = value;
		}

		if ( value ) this.pressedKeys.add(key);
		else this.pressedKeys.delete(key);
	}
}

type ActionChangeListener = ((newState: boolean) => void);

/**
 * Represents the state of an Input Action.
 */
class Action<NameT> {	
	private changeListeners: ActionChangeListener[] = [];
	private _state = false;

	constructor(public name: NameT) {}

	/**
	 * Registers a listener to the change event.
	 * 
	 * @param callback the function to call when a state change happens.
	 * 
	 * @return a function that unregisters this listener.
	 */
	onChange(callback: ActionChangeListener) {
		let disconnected = false;
		this.changeListeners.push((v) => {
			if ( disconnected ) return;
			callback(v);
		});
		return () => {
			disconnected = true;
			this.changeListeners = this.changeListeners.filter(l => l != callback);
		}
	}

	set state(value: boolean) {
		if ( this._state == value ) {
			return;
		}
		this._state = value;
		for ( let cb of this.changeListeners ) {
			cb(value);
		}
	}

	/**
	 * The current state of this action.
	 */
	get state() {
		return this._state;
	}
}