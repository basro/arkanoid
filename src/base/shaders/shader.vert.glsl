attribute vec2 aVertexPosition;
attribute vec2 aTextureCoord;

uniform mat3 uTransform;

varying highp vec2 vTextureCoord;

void main(void) {
	gl_Position = vec4(uTransform * vec3(aVertexPosition,1),1);
	vTextureCoord = aTextureCoord;
}
