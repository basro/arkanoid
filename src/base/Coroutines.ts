
/**
 * A collection of useful coroutines.
 */
export module coro {

	/**
	 * A coroutine is a generator that returns void and only does side effects.
	 */
	export type Coroutine = Generator<undefined, void, unknown>;

	/**
	 * A utility type to pass delta type by reference instead of by value.
	 */
	export interface DeltaTime {
		value: number;
	}

	function* infiniteWait() {
		while(true) yield;
	}
	const infinite = infiniteWait();

	/** A couroutine that never ends and does no side effects */
	export function waitForever() {
		return infinite;
	}

	/**
	 * A coroutine that ends after a certain amount of seconds.
	 * 
	 * Depends on the value of the DeltaTime reference being updated externally between
	 * each yield to reflect the amount of time passed since the last time it was resumed.
	 * 
	 * @param dt a reference to a DeltaTime object that should be updated externally.
	 * @param seconds the number of seconds to wait for.
	 */
	export function* waitForSeconds(dt: DeltaTime, seconds: number) {
		let time = seconds;
		while ( time > 0 ) {
			time -= dt.value;
			yield;
		}
	}
	
	/**
	 * A coroutine that ends when any keyboard key is pressed.
	 */
	export function* waitForAnyKey() {
		let done = false;
		const listener = () => {
			done = true;
			document.removeEventListener("keydown", listener);
		}
		document.addEventListener("keydown", listener);
		while( !done ) yield;
	}

	/**
	 * A coroutine that interpolates a value over a specified amount of time.
	 * 
	 * Depends on the value of the DeltaTime reference being updated externally between
	 * each yield to reflect the amount of time passed since the last time it was resumed.
	 * 
	 * @param dt a reference to a DeltaTime object that should be updated externally.
	 * @param duration the number of seconds it takes to interpolate from start to end.
	 * @param start the starting value.
	 * @param end the target end value.
	 * @param func a callback function that is called on every continuation of the with an updated value.
	 */
	export function* lerp(dt: DeltaTime, duration: number, start:number, end: number, func: (value: number) => void) {
		const increment = (end - start) / duration;
		let time = 0;
		while ( time < duration ) {
			time += dt.value;
			if ( time > duration ) time = duration;
			const value = start + increment * time;
			func(value);
			yield;
		}
	}
	
	/**
	 * A coroutine that will run other coroutines in parallel and ends when any of them ends.
	 * 
	 * @param coroutines a list of coroutines to to run in parallel.
	 */
	export function *race(...coroutines: Coroutine[]) {
		while ( true ) {
			for ( let coroutine of coroutines ) {
				if ( coroutine.next().done ) {
					return;
				}
			}
			yield;
		}
	}

	/**
	 * A coroutine that will run other coroutines in parallel and ends when all of them have ended.
	 * 
	 * @param coroutines a list of coroutines to run in parallel.
	 */
	export function *combine(...coroutines: Coroutine[]) {
		while ( true ) {
			let allDone = true;
			for ( let coroutine of coroutines ) {
				if ( !coroutine.next().done ) {
					allDone = false;
				}
			}
			if ( allDone ) return;
			yield;
		}
	}
}