'use strict';
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = env => {
	const isProd = env && env.prod;
	return {
		devtool: "source-map",
		mode: isProd? "production" : "development",
		entry: {
			arkanoid: './src/arkanoid-main.ts',
			pong: './src/pong-main.ts',
		},
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/,
				},
				{
					test: /\.glsl$/i,
					use: 'raw-loader',
				}
			],
		},
		resolve: {
			extensions: [ '.tsx', '.ts', '.js' ],
		},
		output: {
			filename: '[name].js',
			path: path.resolve(__dirname, 'public'),
		},
		plugins: [
			new CopyPlugin([{ from: 'static', to: './' }]),
		],
	}
}

/*'use strict';
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
	mode: "development",
	//devtool: 'inline-source-map',
	entry: './src/main.ts',
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: 'ts-loader',
				exclude: /node_modules/,
			}
		],
		rules: [
			{
				test: /\.glsl$/i,
				use: 'raw-loader',
			},
		],
	},
	resolve: {
		extensions: [ '.ts', '.tsx', '.js' ]
	},
	plugins: [
		new CopyPlugin([{ from: 'public', to: './' }]),
	],
};*/