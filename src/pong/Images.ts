import { Texture, Graphics, Font } from '../base/Graphics';

export interface Images {
	ballTex: Texture;
	font: Font;
}

export async function loadImages(gfx: Graphics): Promise<Images> {
	const [ballTex] = await Promise.all([
			"pong/circle.png",
		].map(name => gfx.loadTexture("images/"+name))
	);
	const font = await gfx.loadFont("images/font.png", 8,8,1,"abcdefghijklmnopqrstuvwxyz0123456789.<>-O!?_");
	return {ballTex, font};
}
